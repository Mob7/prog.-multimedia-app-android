package com.example.sergio.plsstopaccidentes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sergio.plsstopaccidentes.database.DatabaseHandler;
import com.example.sergio.plsstopaccidentes.sources.Suggestion;

public class enviar_sugerencias_carreteras extends AppCompatActivity {

    private DatabaseHandler db;

    private Button btnAdd;
    private EditText etName;
    private EditText etRoad;
    private EditText etTown;
    private EditText etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_sugerencias_carreteras);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new DatabaseHandler(getApplicationContext());

        btnAdd = (Button) findViewById(R.id.btEnviar);
        etName = (EditText) findViewById(R.id.editNombre);
        etRoad = (EditText) findViewById(R.id.editCarretera);
        etTown = (EditText) findViewById(R.id.editLocalidad);
        etText = (EditText) findViewById(R.id.editComentario);

        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Suggestion sugg = new Suggestion(
                        etName.getText().toString(),
                        etRoad.getText().toString(),
                        etTown.getText().toString(),
                        etText.getText().toString()
                );

                db.addSuggestion(sugg);
                Toast.makeText(getApplicationContext(),R.string.sug_enviada, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(enviar_sugerencias_carreteras.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }



}
