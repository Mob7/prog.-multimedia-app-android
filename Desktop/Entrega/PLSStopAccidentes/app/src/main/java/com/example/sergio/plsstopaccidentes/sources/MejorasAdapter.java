package com.example.sergio.plsstopaccidentes.sources;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sergio.plsstopaccidentes.R;

import java.util.List;

/**
 * Created by sergio on 31/05/2016.
 */
public class MejorasAdapter extends ArrayAdapter{

    private Context context;
    private int layoutId;
    private List<Mejoras> data;

    public MejorasAdapter(Context context, int layoutId, List<Mejoras> data) {

        super(context, layoutId, data);

        this.context = context;
        this.layoutId = layoutId;
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View row = view;
        ItemMejoras item = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutId, parent, false);

            item = new ItemMejoras();
            item.nombre = (TextView) row.findViewById(R.id.tvNombreUsuario);
            item.carretera = (TextView) row.findViewById(R.id.tvCarreteraUsuario);
            item.localidad = (TextView) row.findViewById(R.id.tvLocalidadUsuario);
            item.texto = (TextView) row.findViewById(R.id.tvTextoUsuario);

            row.setTag(item);
        }
        else
        {
            item = (ItemMejoras) row.getTag();
        }

        Mejoras mej = data.get(position);
        item.nombre.setText(mej.getNombre());
        item.carretera.setText(mej.getCarretera());
        item.localidad.setText(mej.getLocalidad());
        item.texto.setText(mej.getTexto());

        return row;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Mejoras getItem(int posicion) {

        return data.get(posicion);
    }

    static class ItemMejoras {

        TextView nombre;
        TextView carretera;
        TextView localidad;
        TextView texto;
    }
}
