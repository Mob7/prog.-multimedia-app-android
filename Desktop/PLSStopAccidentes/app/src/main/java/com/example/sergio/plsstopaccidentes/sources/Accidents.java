package com.example.sergio.plsstopaccidentes.sources;

/**
 * Created by sergio on 29/05/2016.
 */
public class Accidents {
    private String year;
    private String type;
    private String direction;
    private String reason;
    private Double latitud;
    private Double longitud;

    public Accidents(String year, String type, String direction, String reason, Double latitud, Double longitud){
        this.year = year;
        this.type = type;
        this.direction = direction;
        this.reason = reason;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
}
