package com.example.sergio.plsstopaccidentes;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.example.sergio.plsstopaccidentes.sources.Mejoras;
import com.example.sergio.plsstopaccidentes.sources.MejorasAdapter;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class listado_de_mejoras extends AppCompatActivity {

    private MejorasAdapter adapter;
    private List<Mejoras> mejoras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_de_mejoras);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mejoras = new ArrayList<Mejoras>();
        adapter = new MejorasAdapter(this, R.layout.mejoras_item, mejoras);
        ListView lvMejoras = (ListView) findViewById(R.id.lvListadoDeMejoras);
        lvMejoras.setAdapter(adapter);
        adapter.setNotifyOnChange(true);

        WebService webService = new WebService();
        webService.execute();
    }

    private class WebService extends AsyncTask<String, Void, Void> {
        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Mejoras[] baseMejoras = restTemplate.getForObject("http://192.168.1.128:8082" + "/opiniones", Mejoras[].class);
            mejoras.addAll(Arrays.asList(baseMejoras));
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mejoras.clear();
            dialog = new ProgressDialog(listado_de_mejoras.this);
            dialog.setTitle(R.string.cargando);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (dialog != null)
                dialog.dismiss();
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }
    }

}
